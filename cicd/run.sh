#!/usr/bin/env sh

apk update
apk add curl jq

# taken from: https://stackoverflow.com/questions/29109673/is-there-a-way-to-get-the-latest-tag-of-a-given-repo-using-github-api-v3#answer-67404585
latestPhpCsFixerVersion="$(curl -s  "https://api.github.com/repos/PHP-CS-Fixer/PHP-CS-Fixer/tags" | jq -r '.[0].name')"

if [ -z "${latestPhpCsFixerVersion}" ]; then
  echo "Could not fetch latest php-cs-fixer version from github"
  exit 1
fi

echo "latest php cs fixer version: ${latestPhpCsFixerVersion}"

echo "${DOCKER_SECRET}" | docker login -u "${DOCKER_LOGIN}" --password-stdin

imageRepo="acceleratedzerodevelopment/php-cs-fixer"
tagVersion="${imageRepo}:${latestPhpCsFixerVersion}"
tagLatest="${imageRepo}:latest"
platforms="linux/386,linux/amd64,linux/arm64/v8,linux/arm/v6,linux/arm/v7"

docker buildx create --name multiarch --driver docker-container --use
docker buildx build -t "${tagVersion}" -t "${tagLatest}" --pull --no-cache --push --platform="${platforms}" .
