FROM php:cli-alpine

ARG PHP_CS_FIXER_VERSION="v3.54.0"

ADD --chmod=555 "https://github.com/PHP-CS-Fixer/PHP-CS-Fixer/releases/download/${PHP_CS_FIXER_VERSION}/php-cs-fixer.phar" "/php-cs-fixer.phar"

RUN apk del curl

ENTRYPOINT ["php", "/php-cs-fixer.phar"]

CMD ["--version"]
