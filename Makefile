localImageName := local:php-cs-fixer

localImage:
	docker build -t ${localImageName} .

baseCommand:
	docker run --rm -it ${localImageName} $${args:---help}

listCommands:
	make baseCommand args=list
